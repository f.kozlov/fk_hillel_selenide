package api.request;

import api.pojo.body_request.CommentTaskRequest;
import api.pojo.body_request.JiraIssueRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class SendPostRequest {

    public static Response sendPostRequest(RequestSpecification recSpec, JiraIssueRequest jiraIssueRequest, String endpoint,
                                            int expectedStatusCode) {
        return given()
                .spec(recSpec)
                .when()
                .body(jiraIssueRequest)
                .log().all()
                .post(endpoint)
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .extract().response();

    }

    public static Response sendPostRequestForComment(RequestSpecification recSpec, CommentTaskRequest commentTaskRequest, String endpoint,
                                                     int expectedStatusCode) {
        return given()
                .spec(recSpec)
                .when()
                .body(commentTaskRequest)
                .log().all()
                .post(endpoint)
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .extract().response();

    }
}
