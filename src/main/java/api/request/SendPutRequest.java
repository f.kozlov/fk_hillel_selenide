package api.request;

import api.pojo.body_request.AssigneeTaskRequest;
import api.pojo.body_request.CommentTaskRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class SendPutRequest {
    public static Response sendPutRequestForAssignee(RequestSpecification recSpec, AssigneeTaskRequest assigneeTaskRequest, String endpoint,
                                                     int expectedStatusCode) {
        return given()
                .spec(recSpec)
                .when()
                .body(assigneeTaskRequest)
                .log().all()
                .put(endpoint)
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .extract().response();

    }
}
