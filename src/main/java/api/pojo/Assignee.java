package api.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Assignee {
    @JsonProperty("name")
    private String name;

    public Assignee(String name){this.name = name;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
