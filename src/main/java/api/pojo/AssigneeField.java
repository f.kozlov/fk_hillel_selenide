package api.pojo;


public class AssigneeField {
    private Assignee assignee;

    public AssigneeField(String assignee) {
        this.assignee = new Assignee(assignee);
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }
}