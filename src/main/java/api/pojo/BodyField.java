package api.pojo;


public class BodyField {
    private String body;

    public BodyField(String body){
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

