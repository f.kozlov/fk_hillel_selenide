package api.pojo.body_request;

import api.pojo.BodyField;


public class CommentTaskRequest {


    private String body;
    public CommentTaskRequest(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
