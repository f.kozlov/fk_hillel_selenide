package api.pojo.body_request;

import api.pojo.AssigneeField;

public class AssigneeTaskRequest {
private AssigneeField fields;

    public AssigneeTaskRequest (String assignee){
    this.fields = new AssigneeField(assignee);
}

    public AssigneeField getFields() {
        return fields;
    }

    public void setFields(AssigneeField fields) {
        this.fields = fields;
    }
}
