package ui.lms.page_object;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.*;

// page_url = https://lms.ithillel.ua/auth/registration/confirm-email
public class ConfirmEmailPage {

    private SelenideElement emailFilds() {
        return $x("//input[@id='confirm-email-code']");
    }

    private SelenideElement submitBtn() {
        return $x("//button[@type='submit']");
    }

    @Step("Fill in email on the confirm email page")
    public ConfirmEmailPage fillingEmail(String code) {
        emailFilds().setValue(code);
        return this;
    }
    @Step("Click on the submit button on the recommendation page")
    public RecommendationsPage clickBySunmit() {
        submitBtn().click();
        return Selenide.page(RecommendationsPage.class);
    }

}