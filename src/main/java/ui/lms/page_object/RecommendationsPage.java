package ui.lms.page_object;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

// page_url = https://lms.ithillel.ua/store/recommendations
public class RecommendationsPage {

    public static String recommendationsPageUrl = "https://lms.ithillel.ua/store/recommendations";
}