package api.jira;

import api.pojo.body_request.CommentTaskRequest;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static api.request.SendPostRequest.sendPostRequestForComment;
import static api.specification.JiraRequestSpecification.requestSpec;

public class TestCommentTask {

    @Test
    public void TestCommentTask(){
        CommentTaskRequest commentTask = new CommentTaskRequest("This is a comment which was made by test");

        Response response = sendPostRequestForComment(
                requestSpec,
                commentTask,
                "/rest/api/2/issue/U3QAJ201023-1792/comment",
                HttpStatus.SC_CREATED);
    }
}
