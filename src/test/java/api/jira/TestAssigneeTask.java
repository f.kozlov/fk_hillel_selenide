package api.jira;

import api.pojo.body_request.AssigneeTaskRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static api.constnts.ValueName.JIRA_USER;
import static api.request.SendPutRequest.sendPutRequestForAssignee;
import static api.specification.JiraRequestSpecification.requestSpec;

public class TestAssigneeTask {
    @Test
    public void testAssigneeTask(){
        AssigneeTaskRequest assigneeRequest = new AssigneeTaskRequest(JIRA_USER);

        Response response = sendPutRequestForAssignee(
                requestSpec,
                assigneeRequest,
                "/rest/api/2/issue/U3QAJ201023-1792",
                HttpStatus.SC_NO_CONTENT);
    }
}
