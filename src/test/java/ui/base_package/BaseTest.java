package ui.base_package;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static ui.utils.SetUp.setUp;

public class BaseTest {


    @BeforeClass
    public void setUpBrowser() {
        setUp();
    }

    @AfterMethod
    public void closeBrowser() {
        closeWebDriver();
    }
}
