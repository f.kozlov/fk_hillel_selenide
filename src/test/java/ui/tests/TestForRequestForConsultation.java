package ui.tests;

import ui.base_package.BaseTest;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ui.hillel_site.page_object.MainPage;
import ui.utils.Listener;

import static com.codeborne.selenide.Selenide.open;
import static ui.utils.CommonMethods.randomMail;

@Listeners(Listener.class)
public class TestForRequestForConsultation extends BaseTest {

    MainPage mainPage = new MainPage();
    @Test
    @Owner(value = "Kozlov")
    @Description(value = "Test for request for consultation")
    public void requestForConsultation() {
        open("https://ithillel.ua/");
        mainPage.clickByConsultationBnt()
                .filingInputName("Autotest")
                .filingInputMail(randomMail())
                .choosesTelCode("ad")
                .filingInputTelNumber("123456")
                .choosesCourse("Java Pro")
                //.enterComment("This is autotest")
                .clickPrivacyCheckBox()
                .clickBySubmitBtn()
                .checksResultText();
    }
}
