package ui.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import ui.base_package.BaseTest;
import ui.hillel_site.page_object.MainPage;
import ui.lms.page_object.ConfirmEmailPage;
import ui.malinator_service.MailinatorMainPage;
import org.openqa.selenium.WindowType;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.switchTo;
import static ui.lms.page_object.PasswordPage.PASSWORD;
import static ui.lms.page_object.RecommendationsPage.recommendationsPageUrl;
import static ui.utils.CommonMethods.checkingContainsURL;
import static ui.utils.CommonMethods.randomMail;


public class TestRegistration extends BaseTest {

    MainPage mainPage = new MainPage();
    MailinatorMainPage mailinatorMainPage = new MailinatorMainPage();
    ConfirmEmailPage confirmEmailPage = new ConfirmEmailPage();

    private String mail = randomMail();


    private String confirmCod;

    @Test
    @Owner(value = "Kozlov")
    @Description(value = "Test the registration functionality")
    public void testRegistration() {
        System.out.println(mail);
        open("https://ithillel.ua/");
        mainPage.clickByAuthPageBtn()
                .checkFormLoginAndRegistrations()
                .clickByRegistrationBtn()
                .fillingRegistrationForm("AutotestF", "AutotestF", mail, "302346894F")
                .fillingPasswordInput(PASSWORD, PASSWORD);
        switchTo().newWindow(WindowType.TAB);
        switchTo().window(1);
        open("https://www.mailinator.com");

        confirmCod =  mailinatorMainPage.searchMail(mail)
                .openLetterAndGetConfirmCode();
        switchTo().window(0);
        confirmEmailPage.fillingEmail(confirmCod)
                .clickBySunmit();
        checkingContainsURL(recommendationsPageUrl);
    }
}
